# [RabbitMq Demo](https://www.rabbitmq.com)
Basic demo that represents how to use Rabbit mq for job queue in PHP. Could be useful in system load balancing. 


## SETUP PREPARE SYSTEM
 1. [Linux debian setup](https://www.rabbitmq.com/install-debian.html) /  [Windows debian setup](https://www.rabbitmq.com/install-windows.html)
 2. ```sudo rabbitmq-plugins enable rabbitmq_management```
 
## RUN
 1. ```composer install```
 2. ```chmod +x worker.php publisher.php```
 3. ```php publisher.php``` - run publisher (will add demo tasks to list)
 4. ```php worker.php``` - run workers as many as you want, workers will execute tasks
 
## WATCH 
 - url http://localhost:15672
 - user:guest pw:guest
  
## DOCUMENTATION
 1. [ALL](https://www.rabbitmq.com/documentation.html)
   